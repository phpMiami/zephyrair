/*
  ZEPHIR Air Quality Monitor. 
  
  Project participating in Space Apps COVID-19 challenge: Purify the air supply

  Sensors:
   - DHT11: 5V, D1 (GPIO5)
   - Water sensor: 5V, A0
*/

// ArduinoJson - Version: Latest
#include <ArduinoJson.h>
#include <ArduinoJson.hpp>
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>

const char* ssid = "Wifi_SSID";
const char* password = "Wifi_Password";
long long previous_time = 0;

#include <DHT.h>
#define DHTPIN 5 //D1
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);// Initializing DHT11 sensor

const char ledPin = 2;

void setup() {
  Serial.begin(115200);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }
  dht.begin();
  Serial.println("Connected to the WiFi network");
  pinMode(ledPin, OUTPUT);
}

void loop() {
  long long current_time = millis();
  if(current_time - previous_time >= 1000)
  {
    previous_time = current_time;
    if (WiFi.status() == WL_CONNECTED) {
 
      int h = dht.readHumidity();// Reading relative humidity
      float t = dht.readTemperature(); // Reading temperature in degrees
      int water = analogRead(A0); //Reading water steam concentration
      
      Serial.print("Humidity: ");
      Serial.print(h);
      Serial.print(" %\t");
      Serial.print("Temperature: ");
      Serial.print(t);
      Serial.print(" *C\t ");
      Serial.print("Water steam: ");
      Serial.print(water);
      Serial.println(" ppb\t ");
      Serial.println("Sending data to server...");
     
      /* Convert JSON into string  */
      StaticJsonBuffer<200> jsonBuffer;
      JsonObject& doc = jsonBuffer.createObject();
      doc["temp_celsius"] = t;    
      doc["humid_percent"] = h;
      doc["steam_ppb"] = water;
      doc["oxygen_ppb"] = int(0);
      doc["nitrogen_ppb"] = int(0);
      doc["hydrogen_ppb"] = int(0);
      doc["carbon_dioxide_ppb"] = int(0);
      doc["methane_ppb"] = int(0);

      String measure;
      doc.prettyPrintTo(measure);

      HTTPClient http;
      http.begin("the_post_URL","The_HTTPS_server_fingerprint");

      http.addHeader("Content-Type", "application/json");

      digitalWrite(ledPin, HIGH);
      int httpCode = http.POST(measure);
  
      if (httpCode > 0) {
  
        String response = http.getString();
      }
        else
      {
        Serial.print("Error on sending POST Request: ");
        Serial.println(httpCode);
      }
      digitalWrite(ledPin, LOW);
       
      http.end();
    }
  
    else
    {
      Serial.println("Error in WiFi connection");
    }
  }
}
