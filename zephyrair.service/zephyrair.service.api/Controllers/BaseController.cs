﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using zephyrair.service.api.Interfaces;
using zephyrair.service.application.interfaces;
using zephyrair.service.application.Interfaces;

namespace zephyrair.service.api.Controllers
{
    public class BaseController<TDto> : ControllerBase, IController<TDto> where TDto : IDto
    {
        public IService<TDto> Service { get; set; }
    }
}
