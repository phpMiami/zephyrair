﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using zephyrair.service.api.Controllers;
using zephyrair.service.application.Dtos;
using zephyrair.service.application.interfaces;
using zephyrair.service.application.services;

namespace zephyrair.service.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowFromAll")]
    [AllowAnonymous]
    [ApiController]
    public class MeasureController : BaseController<GenericMeasureDto>
    {
        public MeasureController(IService<GenericMeasureDto> service)
        {
            Service = service;
        }

        // POST: api/Measure
        [HttpPost]
        public IActionResult Post([FromBody] MeasureDto value)
        {
            ((MeasureService)Service).SetCurrentMeasure(value);
            return NoContent();
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(((MeasureService)Service).GetCurrentMeasure());
        }
    }
}
