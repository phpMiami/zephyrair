﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using zephyrair.service.application.interfaces;
using zephyrair.service.application.Interfaces;

namespace zephyrair.service.api.Interfaces
{
    public interface IController<TDto> where TDto : IDto
    {
        IService<TDto> Service { get; set; }
    }
}
