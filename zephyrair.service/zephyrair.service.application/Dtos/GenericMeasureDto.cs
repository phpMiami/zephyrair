﻿using System.Text.Json.Serialization;
using zephyrair.service.application.Interfaces;

namespace zephyrair.service.application.Dtos
{
    public class GenericMeasureDto : IDto
    {
        [JsonPropertyName("label")]
        public string Label { get; set; }
        [JsonPropertyName("value")]
        public object Value { get; set; }
    }
}
