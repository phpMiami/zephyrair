﻿using System.Text.Json.Serialization;

namespace zephyrair.service.application.Dtos
{
    public class MeasureDto
    {
        [JsonPropertyName("temp_celsius")]
        public float Temperature { get; set; }
        [JsonPropertyName("humid_percent")]
        public int Humidity { get; set; }
        [JsonPropertyName("steam_ppb")]
        public int Steam { get; set; }
        [JsonPropertyName("oxygen_ppb")]
        public int Oxigen { get; set; }
        [JsonPropertyName("nitrogen_ppb")]
        public int Nitrogen { get; set; }
        [JsonPropertyName("hydrogen_ppb")]
        public int Hydrogen { get; set; }
        [JsonPropertyName("carbon_dioxide_ppb")]
        public int CarbonDioxide { get; set; }
        [JsonPropertyName("methane_ppb")]
        public int Methane { get; set; }
    }
}