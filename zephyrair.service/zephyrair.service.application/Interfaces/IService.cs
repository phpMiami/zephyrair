﻿using zephyrair.service.application.Interfaces;

namespace zephyrair.service.application.interfaces
{
    public interface IService<TDto> where TDto: IDto
    {
    }
}
