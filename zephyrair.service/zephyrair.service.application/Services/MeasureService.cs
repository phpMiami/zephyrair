﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using zephyrair.service.application.Dtos;
using zephyrair.service.application.interfaces;

namespace zephyrair.service.application.services
{
    public class MeasureService: IService<GenericMeasureDto>
    {
        public MeasureDto Measure { get; set; }

        private DateTime previousDateMinutes { get; set; }

        public void SetCurrentMeasure(MeasureDto measureDto)
        {
            Measure = measureDto;
        }

        public List<GenericMeasureDto> GetCurrentMeasure()
        {
            var label = DateTime.Now.ToString("HH:mm");
            List<GenericMeasureDto> measures = new List<GenericMeasureDto>
            {
               new GenericMeasureDto
               {
                   Label = label,
                   Value = Measure?.Temperature
               },

               new GenericMeasureDto
               {
                   Label = label,
                   Value = Measure?.Humidity
               },
               new GenericMeasureDto
               {
                   Label = label,
                   Value = Measure?.Steam
               },
            };
            return measures;
        }
    }
}
