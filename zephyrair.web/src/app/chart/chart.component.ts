import { Component, Input, AfterViewInit } from '@angular/core';
import { DashboardService } from '../dashboard/dashboard.service';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements AfterViewInit {
  @Input() dataIndex: number;
  @Input() chartTitle: string;
  @Input() chartRgbaColor: string;
  @Input() chartId: string;
  public chart: any = null;
  service: DashboardService;
  constructor(service: DashboardService) {
    this.service = service;   
   }

   

  ngAfterViewInit(): void {
    let rgbaNoAlpha = this.chartRgbaColor.replace("0.3","1");
    this.chart = new Chart(this.chartId, {
      type: 'line',
      data: {
       labels: [],
       datasets: [
         {
        label: this.chartTitle,
        fill: false,
        data: [],
        backgroundColor: this.chartRgbaColor,
        borderColor: rgbaNoAlpha
         }
       ]
        },
        options: {
       tooltips: {
        enabled: true
       },
       legend: {
        display: true,
        position: 'bottom',
        labels: {
         fontColor: 'black'
        }
       },
       scales: {
         yAxes: [{
          ticks: {
           fontColor: "black"
          }
         }],
         xAxes: [{
        ticks: {
         fontColor: "black",
         beginAtZero: true
        }
         }]
       }
        }
     });

    this.service.$dashboard.subscribe(res => 
    {
      if(!this.chart.data.labels.includes(res[this.dataIndex].label)){
        this.chart.data.labels.push(res[this.dataIndex].label);
      }
      else{
        this.chart.data.labels.push("");
      }
      this.chart.data.datasets[0].data.push(res[this.dataIndex].value);
      if(this.chart.data.labels.length > 60) {
        this.chart.data.labels.shift();
        this.chart.data.datasets[0].data.shift();
      }
      this.chart.update();
    });
  }

}
