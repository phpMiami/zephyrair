import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { DashboardService } from './dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  intervalRef: any;
  service: DashboardService;
  constructor(service: DashboardService) {  
    this.service = service;   
   };

  ngOnInit(): void {
    this.intervalRef = setInterval(function() { 
      this.service.update();     
    }.bind(this), 500);
  }

  ngOnDestroy(): void {
    clearInterval(this.intervalRef);
   }
}
