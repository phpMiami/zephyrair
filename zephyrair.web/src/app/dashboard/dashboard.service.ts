import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { ChartValue } from '../interfaces/chart-value';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  private measureCount: number = 3;
  private dashboard: Subject<ChartValue[]>;
  $dashboard: Observable<ChartValue[]>;  
  private previousCharValues: ChartValue[];    

  constructor(private http: HttpClient) { 
    this.dashboard = new Subject<ChartValue[]>();
    this.$dashboard = this.dashboard.asObservable();

 
  }

  update(): void{

    //Mock data
    let chartValues: ChartValue[] = [];
    let date = new Date(); 
    for (let index = 0; index < this.measureCount; index++) {
      let lastValue;
      if(this.previousCharValues){
        lastValue = this.previousCharValues[this.previousCharValues.length - 1].value;
      }
      else
      {
        lastValue = Math.floor(Math.random()*30+10)
      }
      let increment = 0;
      let newValue;
      let multiply = 1;
      let random100 = Math.random()*100+1;
      if(random100 < 85){
        increment = 0;
      }
      else if(random100 < 95)
      {
        increment = 1;
      }
      else if(random100 < 98)
      {
        increment = 2;
      }
      else if(random100 < 99)
      {
        increment = 3;
      }
      else if(random100 < 100)
      {
        increment = 4;
      } 
      else{
        increment = 5;
      }

      if(Math.random() >= 0.5){
        multiply = -1;
      }
      newValue = lastValue + increment * multiply;

      if(newValue < 10){
        newValue = 10;
      }
      else if(newValue > 40){
        newValue = 40;
      }
      chartValues.push({label: date.getHours() +":"+date.getMinutes(), value: Math.floor(newValue)});      
    }
    this.previousCharValues = chartValues;

    this.dashboard.next(chartValues);

    // Call API to retrieve current values and notify observers
    // const httpOptions = {
    //   headers: new HttpHeaders({ 
    //     'Access-Control-Allow-Origin':'*',
    //     'accept': '*'
    //   })
    // };
    
    // this.http.get<ChartValue[]>("https://zephyrairservice.azurewebsites.net/api/Measure", httpOptions)
    // .subscribe(heroes => {
    //   this.dashboard.next(heroes)
    // });
    


  }
}
