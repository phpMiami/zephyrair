export interface ChartValue {
    label: string;
    value: number;
}
